﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DataContext
        : DbContext
    {
        public DbSet<Preference> Preferences { get; set; }
        
        public DbSet<Role> Roles { get; set; }
        
        public DbSet<Employee> Employees { get; set; }
        
        public DbSet<Partner> Partners { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DataContext()
        {
            
        }
        
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MainPromoCodePartnerPreference>()
                .HasKey(bc => new {bc.PartnerId, bc.PreferenceId});
            modelBuilder.Entity<MainPromoCodePartnerPreference>()
                .HasOne(bc => bc.Partner)
                .WithMany(b => b.MainPromoCodePreferences)
                .HasForeignKey(bc => bc.PartnerId);
            modelBuilder.Entity<MainPromoCodePartnerPreference>()
                .HasOne(bc => bc.Preference)
                .WithMany()
                .HasForeignKey(bc => bc.PreferenceId);

            //modelBuilder.Entity<MainPromoCodePartnerPreference>().ToTable("MainPromoCodePartnerPreferences");

            //modelBuilder.Entity<PartnerPromoCodeLimit>().ToTable("PartnerPromoCodeLimits");
        }
    }
}