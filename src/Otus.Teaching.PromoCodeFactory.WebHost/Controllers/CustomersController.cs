﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<PromoCode> _promoCodeRepository;

        /// <summary>
        /// Контроллер класса Customers/Покупатели
        /// </summary>
        /// <param name="customerRepository">Переменная для работы с данными сущности "Покупатель"</param>
        /// <param name="preferenceRepository">Переменная для работы с данными сущности "Предпочтения"</param>
        /// <param name="promoCodeRepository">Переменная для работы с данными сущности "Промокод"</param>
        public CustomersController(IRepository<Customer> customerRepository, 
            IRepository<Preference> preferenceRepository, IRepository<PromoCode> promoCodeRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _promoCodeRepository = promoCodeRepository;
        }
        
        /// <summary>
        /// Получить список всех покупателей
        /// </summary>
        /// <returns>Список всех пользователей</returns>
        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            var customers =  await _customerRepository.GetAllAsync();

            var response = customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            return Ok(response);
        }
        
        /// <summary>
        /// Получить данные о покупателе 
        /// </summary>
        /// <param name="id">Уникальный идентификатор покупателя</param>
        /// <returns>Данные о покупателе</returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer =  await _customerRepository.GetByIdAsync(id);
            
            if (customer == null)
            {
                return NotFound($"Покупателей с ID ({id}) не найдено");
            }

            var response = new CustomerResponse(customer);

            return Ok(response);
        }
        
        /// <summary>
        /// Создание записи о покупателе
        /// </summary>
        /// <param name="request">Класс запроса для создания записи о покупателе или их изменения</param>
        /// <returns>Данные о покупателе</returns>
        [HttpPost]
        public async Task<ActionResult<CustomerResponse>> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            //Получаем предпочтения из бд и сохраняем большой объект
            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(request.PreferenceIds);

            Customer customer = CustomerMapper.MapFromModel(request, preferences);
            
            await _customerRepository.AddAsync(customer);

            return CreatedAtAction(nameof(GetCustomerAsync), new {id = customer.Id}, customer.Id);
        }

        /// <summary>
        /// Изменение данных покупателя
        /// </summary>
        /// <param name="id">Уникальный идентификатор покупателя</param>
        /// <param name="request">Класс запроса для создания записи о покупателе или их изменения</param>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);
            
            CustomerMapper.MapFromModel(request, preferences, customer);

            await _customerRepository.UpdateAsync(customer);

            return NoContent();
        }

        /// <summary>
        /// Удалить данные о покупателе
        /// </summary>
        /// <param name="id">Уникальный идентификатор покупателя</param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            
            customer.PromoCodes.Select(async promoCode =>await _promoCodeRepository.DeleteAsync(promoCode));

            await _customerRepository.DeleteAsync(customer);

            return NoContent();
        }
    }
}