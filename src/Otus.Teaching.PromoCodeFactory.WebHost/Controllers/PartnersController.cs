﻿﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Core.Application.Dto;
using Otus.Teaching.PromoCodeFactory.Core.Application.Exceptions;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Партнеры
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PartnersController
        : ControllerBase
    {
        private readonly IPartnerService _partnerService;

        public PartnersController(IPartnerService partnerService)
        {
            _partnerService = partnerService;
        }

        /// <summary>
        /// Получить список всех партнеров
        /// </summary>
        /// <returns>Список всех партнеров</returns>
        [HttpGet]
        public async Task<ActionResult<List<PartnerResponseDto>>> GetPartnersAsync()
        {
            var response = await _partnerService.GetPartnersAsync();

            return Ok(response);
        }
        
        /// <summary>
        /// Получить данные о партнере
        /// </summary>
        /// <param name="id">Уникальный идентификатор партнера</param>
        /// <returns>Данные о партнере</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<List<PartnerResponseDto>>> GetPartnersAsync(Guid id)
        {
            try
            {
                var response = await _partnerService.GetPartnerByIdAsync(id);
                return Ok(response);
            }
            catch (EntityNotFoundException)
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Получить данные о лимите партнера
        /// </summary>
        /// <param name="id">Уникальный идентификатор партнера</param>
        /// <param name="limitId">Уникальный идентификатор лимита</param>
        /// <returns></returns>
        [HttpGet("{id}/limits/{limitId}")]
        public async Task<ActionResult<PartnerPromoCodeLimitResponseDto>> GetPartnerLimitAsync(Guid id, Guid limitId)
        {
            try
            {
                var response = await _partnerService.GetPartnerLimitAsync(id, limitId);
                return Ok(response);
            }
            catch (EntityNotFoundException)
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Установить лимит для промокодов партнеру
        /// </summary>
        /// <param name="id">Уникальный идентификатор партнера</param>
        /// <param name="requestDto">Класс запроса для установления лимита промокодов партнеру</param>
        /// <returns></returns>
        [HttpPost("{id}/limits")]
        public async Task<IActionResult> SetPartnerPromoCodeLimitAsync(Guid id, SetPartnerPromoCodeLimitRequestDto requestDto)
        {
            try
            {
                var newLimitId = await _partnerService.SetPartnerPromoCodeLimitAsync(id, requestDto);
                return CreatedAtAction(nameof(GetPartnerLimitAsync), new {id = id, limitId = newLimitId.ToString()}, null);
            }
            catch (EntityNotFoundException)
            {
                return NotFound();
            }
            catch(ChangePartnerLimitException)
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Удалить лимит промокодов для партнера
        /// </summary>
        /// <param name="id">Уникальный идентификатор партнера</param>
        /// <returns></returns>
        [HttpPost("{id}/canceledLimits")]
        public async Task<IActionResult> CancelPartnerPromoCodeLimitAsync(Guid id)
        {
            await _partnerService.CancelPartnerPromoCodeLimitAsync(id);
            
            return NoContent();
        }
    }
}