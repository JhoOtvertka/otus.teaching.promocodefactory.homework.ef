﻿﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;


namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public class CustomerMapper
    {

        public static Customer MapFromModel(CreateOrEditCustomerRequest model, IEnumerable<Preference> preferences, Customer customer = null)
        {
            if(customer == null)
            {
                customer = new Customer{ Id = Guid.NewGuid() };
            }
            
            customer.FirstName = model.FirstName;
            customer.LastName = model.LastName;
            customer.Email = model.Email;
            customer.CustomerPreferences = new List<CustomerPreference>();

            foreach(var preference in preferences)
            {
                customer.CustomerPreferences.Add(new CustomerPreference { CustomerId = customer.Id, PreferenceId = preference.Id });
            }

            return customer;
        }
    }
}
