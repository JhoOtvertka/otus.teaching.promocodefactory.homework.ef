﻿﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public class PromoCodeMapper
    {
        public static PromoCode MapGromModel(GivePromoCodeRequest request, Preference preference) {

            var promocode = new PromoCode
            {
                Id = Guid.NewGuid(),

                PartnerName = request.PartnerName,
                Code = request.PromoCode,
                ServiceInfo = request.ServiceInfo,

                BeginDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(30),

                Preference = preference,
                PreferenceId = preference.Id,
            };

            return promocode;
        }
    }
}
