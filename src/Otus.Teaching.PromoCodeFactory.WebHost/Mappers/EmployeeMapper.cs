﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public class EmployeeMapper
    {

        public static Employee MapFromModel(CreateOrEditEmployeeRequest model, Employee employee = null)
        {
            if(employee == null)
            {
                employee = new Employee 
                { 
                    Id = Guid.NewGuid()
                };
            }

            employee.FirstName = model.FirstName;
            employee.LastName = model.LastName;
            employee.Email = model.Email;
            employee.AppliedPromocodesCount = model.AppliedPromocodesCount;
            employee.RoleId = model.RoleId;

            return employee;
        }
    }
}
