﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class CustomerResponse
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public List<PreferenceResponse> Preferences { get; set; }

        public List<PromoCodeShortResponse> PromoCodes { get; set; }

        public CustomerResponse()
        {
            
        }

        public CustomerResponse(Customer customer)
        {
            Id = customer.Id;
            Email = customer.Email;
            FirstName = customer.FirstName;
            LastName = customer.LastName;

            Preferences = customer.CustomerPreferences.Select(customerPreference => new PreferenceResponse()
            {
                Id = customerPreference.Preference.Id,
                Name = customerPreference.Preference.Name
            }).ToList();

            PromoCodes = customer.PromoCodes.Select(promoCode => new PromoCodeShortResponse() { 
                Id = promoCode.Id,
                Code = promoCode.Code,
                ServiceInfo = promoCode.ServiceInfo,
                BeginDate = promoCode.BeginDate.ToString(),
                EndDate = promoCode.EndDate.ToString(),
                PartnerName = promoCode.PartnerName
            }).ToList();
        }
    }
}